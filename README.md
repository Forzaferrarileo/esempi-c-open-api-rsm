# README #

Repository con i vari esempi in c++ per prelevare gli open data forniti da roma servizi per la mobilità

** Esempi : **

* percorsi.cpp : restituisce i dati del metodo paline.Percorsi , ovvero i percorsi di una determinata linea

```

id_richiesta : dbc377a46fd16a065b1ae59db0eadc8b
abilitata : 1
id_news : -1
monitorata : 1
percorsi : 
	percorso 0 : 
		id_percorso: 51963
		descrizione: Temporanea
		capolinea: Roccalumera/Borghesiana
	percorso 1 : 
		id_percorso: 52564
		descrizione: Temporanea
		capolinea: Orafi
```



**Requisiti**

libreria xmlrpc for c c++ installata : http://xmlrpc-c.sourceforge.net/

**Compilazione degli esempi**

( su distro linux )

percorsi.cpp : ``` g++ -o percorsi percorsi.cpp  `xmlrpc-c-config c++2 client --libs` ```