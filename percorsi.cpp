#include <cstdlib>
#include <string>
#include <iostream>

using namespace std;

#include <xmlrpc-c/girerr.hpp>
#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/client_simple.hpp>

#define DEV_KEY "tua_dev_key"

int
main(int argc, char **) {

	try {

	        string const authServer("http://muovi.roma.it/ws/xml/autenticazione/1");
	        string const authMethod("autenticazione.Accedi");
	
	        xmlrpc_c::clientSimple authClient;
	        xmlrpc_c::value result;
        
	        authClient.call(authServer, authMethod, "ss", &result, DEV_KEY, "");
	
	        string str = xmlrpc_c::value_string(result);


		cout << "token: " << str << endl;



        	string const palineServer("http://muovi.roma.it/ws/xml/paline/7");
	        string const palineMethod("paline.Percorsi");

		xmlrpc_c::clientSimple palineClient;
		xmlrpc_c::value paline;

		palineClient.call(palineServer, palineMethod, "sss", &paline, str.c_str() , "056", "it");

	  	map<string, xmlrpc_c::value> mymap , risposta ;

		xmlrpc_c::value_struct palineStruct = xmlrpc_c::value_struct(paline);

		mymap = palineStruct.cvalue();

		cout<< "id_richiesta : " << xmlrpc_c::value_string(mymap["id_richiesta"]).cvalue() << endl;

		risposta = xmlrpc_c::value_struct(mymap["risposta"]).cvalue();

		//I tipi dei vari elementi sono fissati rigidamente seguendo il layout della risposta

		map<string, xmlrpc_c::value>::iterator it = risposta.begin();

		cout<< it->first << " : " << xmlrpc_c::value_boolean(risposta["abilitata"]).cvalue() << endl;
		++it;
		cout<< it->first << " : " << xmlrpc_c::value_int(risposta["id_news"]).cvalue() << endl;
		++it;
		cout<< it->first << " : " << xmlrpc_c::value_int(risposta["monitorata"]).cvalue() << endl;
		++it;
		cout<< it->first << " : " << endl;

		vector<xmlrpc_c::value> percorsi;

		percorsi = xmlrpc_c::value_array(risposta["percorsi"]).cvalue();
		
		int numeroPercorsi = percorsi.size();

		for( int i = 0; i < numeroPercorsi; i++){

			map<string, xmlrpc_c::value> percorso = xmlrpc_c::value_struct(percorsi[i]).cvalue();
			cout << "	percorso " << i << " : " << endl;
			cout << "		id_percorso: " << xmlrpc_c::value_string(percorso["id_percorso"]).cvalue() << endl;
			cout << "		descrizione: " << xmlrpc_c::value_string(percorso["descrizione"]).cvalue() << endl;
			cout << "		capolinea: " << xmlrpc_c::value_string(percorso["capolinea"]).cvalue() << endl;

		}


	} catch (exception const& e) {
	        cerr << "Client threw error: " << e.what() << endl;
	} catch (...) {
		cerr << "Client threw unexpected error." << endl;
	}
	

	return 0;
}
